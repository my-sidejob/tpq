@extends('layouts.app')

@section('title', 'Santri')

@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Santri</h4>
            </div>
            <div class="card-body">
                @if($type == 'default')
                    <a href="{{ route('santri.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Santri</a>
                @else
                    <p>Berikut adalah data santri anda yang terdaftar</p>
                    <a href="{{ route('santri.daftar') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Daftar Santri Baru</a>
                @endif
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Santri</th>
                            <th>TTL</th>
                            <th>J. Kel</th>
                            <th>Status</th>
                            <th>Wali</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($santri as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->tempat_lhr . ', ' . $row->formatDate() }}</td>
                                <td>{{ strtoupper($row->j_kel) }}</td>
                                <td>{!! $row->getStatus() !!}</td>
                                <td>{!! $row->wali->nama !!}</td>
                                @if($type == 'default')
                                <td>
                                    <form action="{{ route('santri.destroy', $row->id) }}" method="post">
                                        @if(auth()->user()->level == 2)
                                            <a href="{{ route('santri.grafik', $row->id) }}" class="btn btn-sm btn-info">Report</a>
                                        @endif
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('santri.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                        <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                                @else
                                <td>
                                    <a href="{{ route('wali.santri.grafik', $row->id) }}" class="btn btn-sm btn-info">Grafik</a>
                                </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Belum ada data</td>
                            </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
