@extends('layouts.app')

@section('title', 'Form santri')

@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form santri</h4>
            </div>
            <div class="card-body">
                <form action="{{  Auth::guard('web')->check() ? (!isset($santri->id)) ? route('santri.store') : route('santri.update', $santri->id) : route('santri.register') }}" method="post">
                    @csrf
                    @isset($santri->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama Wali</label>
                        @if(Auth::guard('web')->check())
                        <select name="wali_id" class="form-control mySelect">
                            <option value="">- Pilih Wali -</option>
                            @foreach($wali as $option)
                                <option value="{{ $option->id }}" {{ old('wali_id', $santri->wali_id) == $option->id ? 'selected' : '' }} class="text-dark">{{ $option->nama }}</option>
                            @endforeach
                        </select>
                        @else
                            <input type="text" name="" class="form-control" readonly value="{{ Auth::guard('wali')->user()->nama }}">
                            <input type="hidden" name="wali_id" class="form-control" readonly value="{{ Auth::guard('wali')->user()->id }}">
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Nama Santri</label>
                        <input type="text" placeholder="Nama santri" class="form-control" name="nama" value="{{ old('nama', $santri->nama) }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" placeholder="Tempat Lahir" class="form-control" name="tempat_lhr" value="{{ old('tempat_lhr', $santri->tempat_lhr) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="text" placeholder="Tanggal Lahir" class="form-control datepicker" name="tgl_lhr" value="{{ old('tgl_lhr', $santri->tgl_lhr) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label><br>
                        <label for="l">
                            <input type="radio" id="l" name="j_kel" value="l" {{ old('j_kel', $santri->j_kel) == 'l' ? 'checked' : '' }}> Laki - Laki
                        </label> &nbsp;
                        <label for="p">
                            <input type="radio" id="p" name="j_kel" value="p" {{ old('j_kel', $santri->j_kel) == 'p' ? 'checked' : '' }}> Perempuan
                        </label>
                    </div>

                    @if(Auth::guard('web')->check())
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1" {{ old('status', $santri->status) == "1" ? 'selected' : '' }} class="text-dark">Aktif</option>
                            <option value="0" {{ old('status', $santri->status) == "0" ? 'selected' : '' }} class="text-dark">Tidak Aktif</option>
                        </select>
                    </div>
                    @else
                        <input type="hidden" name="status" class="form-control" readonly value="0">
                    @endif

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
