@extends('layouts.app')

@section('title', 'Grafik')

@section('content')

<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Report Santri</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Identitas Santri</h2>
                        <br>
                        <table class="table table-sm">
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>{{ $santri->nama }}</td>
                            </tr>
                            <tr>
                                <td>TTL</td>
                                <td>:</td>
                                <td>{{ $santri->tempat_lhr . ', ' . $santri->tgl_lhr }}</td>
                            </tr>
                            <tr>
                                <td>J. Kel</td>
                                <td>:</td>
                                <td>{{ $santri->j_kel }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td>{!! $santri->getStatus() !!}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h2>Absensi Santri</h2>
                        <br>
                        <select name="bulan" id="" class="form-control bulan-option">
                            @foreach(getBulan() as $key => $option)
                                <option value="{{ $key }}" {{ $key == $bulanya ? 'selected' : '' }} class="text-dark">{{ $option }}</option>
                            @endforeach
                        </select>
                        <br>
                        <table class="table table-sm">
                            <tr>
                                <td>Tanggal</td>
                                <td>Status</td>
                                <td>Keterangan</td>
                            </tr>
                            @foreach($absensi as $row)
                                <tr>
                                    <td>{{ $row->tgl }}</td>
                                    <td>{!! $row->getAbsenStatus() !!}</td>
                                    <td>{{ $row->keterangan }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-6">
                        <h2>Grafik Nilai Santri</h2>
                        <canvas id="myChart" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>
    $('.bulan-option').change(function(){
        var val = $(this).val();
        window.location.href = "{{ url()->current() }}" + "?b=" + val;
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js" integrity="sha512-GMGzUEevhWh8Tc/njS0bDpwgxdCJLQBWG3Z2Ct+JGOpVnEmjvNx6ts4v6A2XJf1HOrtOsfhv3hBKpK9kE5z8AQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
const ctx = document.getElementById('myChart').getContext('2d');

bulan = "{{ $bulan }}".split(",");
nilai = "{{ $nilai }}".split(",");
console.log(bulan);
const myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: bulan,
        datasets: [{
            label: 'Rata Rata Nilai',
            data: nilai,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>

@endpush
