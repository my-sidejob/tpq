@extends('layouts.app')

@section('title', 'Jadwal')



@section('content')
<div class="row ">
    @if(Auth::guard('web')->check())
    <div class="col-12">
        <a href="{{ route('jadwal.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Jadwal</a>
    </div>
    @endif
    @foreach(getHari() as $hari)
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>{{ $hari }}</h4>
            </div>
            <div class="card-body">
                <p>Jadwal dan kegiatan hari {{ $hari }}</p>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kegiatan</th>
                            <th>Jam</th>
                            @if(Auth::guard('web')->check())
                            <th></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @forelse (getJadwal($hari) as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->keterangan }}</td>
                                <td>{{ $row->jam_mulai . ' - ' . $row->jam_selesai }}</td>
                                @if(Auth::guard('web')->check())
                                <td>
                                    <form action="{{ route('jadwal.destroy', $row->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('jadwal.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                        <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Tidak ada jadwal</td>
                            </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endforeach
</div>



@endsection
