@extends('layouts.app')

@section('title', 'Form jadwal')

@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form jadwal</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($jadwal->id)) ? route('jadwal.store') : route('jadwal.update', $jadwal->id) }}" method="post">
                    @csrf
                    @isset($jadwal->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Hari</label>
                        <select name="hari" class="form-control mySelect">
                            <option value="">- Pilih Hari -</option>
                            @foreach(getHari() as $option)
                                <option value="{{ $option }}" {{ old('hari', $jadwal->hari) == $option ? 'selected' : '' }} class="text-dark">{{ $option }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jam Mulai</label>
                                <input type="time" placeholder="Jam Mulai" class="form-control" name="jam_mulai" value="{{ old('jam_mulai', $jadwal->jam_mulai) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jam Selesai</label>
                                <input type="time" placeholder="Jam Selesai" class="form-control" name="jam_selesai" value="{{ old('jam_selesai', $jadwal->jam_selesai) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" placeholder="Keterangan" class="form-control" name="keterangan" value="{{ old('keterangan', $jadwal->keterangan) }}">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
