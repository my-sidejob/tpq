@extends('layouts.app')

@section('title', 'Form absensi')

@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form absensi</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('absensi.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" placeholder="Masukan Tanggal" class="form-control datepicker" name="tgl" value="{{ old('tgl', $absensi->tgl) }}">
                    </div>
                    <div class="form-group">
                        <p>Data Santri</p>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Absensi</th>
                                    <th>Ket</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($santri as $key => $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>
                                        <input type="hidden" value="{{ $row->id }}" name="santri[id][{{ $key }}]">
                                        <label for="alpha-{{ $key }}">
                                            <input type="radio" name="santri[status][{{ $key }}]" value="0" id="alpha-{{ $key }}" checked> Alpha
                                        </label> &nbsp;
                                        <label for="hadir-{{ $key }}">
                                            <input type="radio" name="santri[status][{{ $key }}]" value="1" id="hadir-{{ $key }}"> Hadir
                                        </label> &nbsp;
                                        <label for="izin-{{ $key }}">
                                            <input type="radio" name="santri[status][{{ $key }}]" value="2" id="izin-{{ $key }}"> Izin
                                        </label> &nbsp;
                                    </td>
                                    <td><input type="text" name="santri[keterangan][{{ $key }}]"></td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>

                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
