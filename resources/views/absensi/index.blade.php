@extends('layouts.app')

@section('title', 'Absensi')

@section('content')
<div class="row ">
    @if(Auth::guard('web')->check())
    <div class="col-12">
        <a href="{{ route('absensi.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Absensi</a>
    </div>
    @endif
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Cari Absensi</h4>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-group">
                        <input type="text" name="tanggal" id="" placeholder="Masukan tanggal" class="form-control datepicker" value="{{ request()->get('tanggal') ?? '' }}">
                    </div>
                    <div class="form-group text-right">
                        <input type="submit" value="Cari" class="btn btn-outline-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@if(isset($_GET['tanggal']))
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Absensi Tanggal {{ request()->get('tanggal') }}</h4>
            </div>
            <div class="card-body">
                <p>Anda dapat melihat dan mengubah data absensi pada tabel dibawah</p>
                <form action="{{ route('absensi.store') }}" method="post">
                    @csrf
                    <input type="hidden" placeholder="Masukan Tanggal" class="form-control datepicker" name="tgl" value="{{ request()->get('tanggal') }}">
                    <input type="hidden" placeholder="Masukan Tanggal" class="form-control datepicker" name="edit" value="true">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Absensi</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($absensi as $key => $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->santri->nama }}</td>
                                <td>
                                    @if(Auth::guard('web')->check())
                                    <input type="hidden" name="santri[id][{{ $key }}]" value="{{ $row->santri_id }}">
                                    <label for="alpha-{{ $key }}">
                                        <input type="radio" name="santri[status][{{ $key }}]" value="0" id="alpha-{{ $key }}" {{ $row->status == "0" ? 'checked' : '' }}> Alpha
                                    </label> &nbsp;
                                    <label for="hadir-{{ $key }}">
                                        <input type="radio" name="santri[status][{{ $key }}]" value="1" id="hadir-{{ $key }}" {{ $row->status == "1" ? 'checked' : '' }}> Hadir
                                    </label> &nbsp;
                                    <label for="izin-{{ $key }}">
                                        <input type="radio" name="santri[status][{{ $key }}]" value="2" id="izin-{{ $key }}" {{ $row->status == "2" ? 'checked' : '' }}> Izin
                                    </label> &nbsp;
                                    @else
                                        {!! $row->getAbsenStatus() !!}
                                    @endif
                                </td>
                                <td>
                                    @if(Auth::guard('web')->check())
                                    <input type="text" name="santri[keterangan][{{ $key }}]" value="{{ $row->keterangan }}">
                                    @else
                                    {{ $row->keterangan }}
                                    @endif
                                </td>
                            </tr>

                            @empty
                            <tr>
                                <td colspan="5">Belum ada data absensi</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @if(Auth::guard('web')->check())
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
