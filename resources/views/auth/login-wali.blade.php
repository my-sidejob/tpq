@extends('layouts.app')

@section('title', 'Login Wali')

@section('content')
<style>
    .bg-login {
        background-image:url("{{ asset('img/bg-login-wali.jpg') }}");
        background-size: cover;
    }
</style>
<div class="page login-page bg-primary bg-login">
    <div class="container">

        <div class="form-outer text-center d-flex align-items-center">
            <div class="form-inner w-100">
                @if($detailProfile != null)
                    <img src="{{ asset('img/logo/'.$detailProfile->logo) }}" alt="person" width="100" class="">
                @else
                    <img src="{{ asset('img/icon.jpg') }}" alt="person" width="100" class="">
                @endif
                <br><br>
                <div class="logo text-uppercase"><span></span><strong class="text-primary">LOGIN</strong></div>
                <p>Login Wali Santri</p>
                @if($errors->any())
                    <hr>
                    @foreach($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach

                @endif
                <form method="post" class="text-left" action="{{ route('login.wali') }}">
                    @csrf
                    <div class="form-group-material">
                        <input id="login-email" type="text" name="email" required data-msg="Please enter your email" class="input-material">
                        <label for="login-email" class="label-material">Email</label>
                    </div>
                    <div class="form-group-material">
                        <input id="login-password" type="password" name="password" required="" data-msg="Please enter your password" class="input-material">
                        <label for="login-password" class="label-material">Password</label>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" value="LOGIN" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="copyrights text-center">
            <p>&copy; 2021 </p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
        </div>
    </div>
</div>

@endsection
