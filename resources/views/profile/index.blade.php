@extends('layouts.app')

@section('title', 'Profile')



@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Profile</h4>
            </div>
            <div class="card-body">
                @if($profile == null)
                    <a href="{{ route('profile.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Profile</a>
                    <p>Silahkan tambah profile TPQ anda</p>
                @else
                    <a href="{{ route('profile.edit', $profile->id) }}" class="btn btn-warning text-white btn-sm mb-4"><i class="fa fa-cog"></i> Edit Profile</a>
                    <br>
                    <img src="{{ asset('img/logo/' . $profile->logo) }}" alt="logo" width="150">
                    <br><br>
                    <table class="table">
                        <tr>
                            <td style="">Nama TPQ</td>
                            <td  style="">{{ $profile->nama }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $profile->email }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>{{ $profile->alamat }}</td>
                        </tr>
                        <tr>
                            <td>No. Telp</td>
                            <td>{{ $profile->no_telp }}</td>
                        </tr>
                        <tr>
                            <td>Deskripsi</td>
                            <td>{{ $profile->deskripsi }}</td>
                        </tr>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>



@endsection
