@extends('layouts.app')

@section('title', 'Form Profile')

@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Profile</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($profile->id)) ? route('profile.store') : route('profile.update', $profile->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @isset($profile->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama TPQ</label>
                        <input type="text" placeholder="Nama TPQ" class="form-control" name="nama" value="{{ old('nama', $profile->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" placeholder="Email" class="form-control" name="email" value="{{ old('email', $profile->email) }}">
                    </div>
                    <div class="form-group">
                        <label>No Telp</label>
                        <input type="text" placeholder="No Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $profile->no_telp) }}">
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" placeholder="logo" class="form-control" name="logo" value="{{ old('logo', $profile->logo) }}">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea placeholder="Alamat" class="form-control" name="alamat">{{ old('alamat', $profile->alamat) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea placeholder="Deskripsi" class="form-control" name="deskripsi">{{ old('deskripsi', $profile->deskripsi) }}</textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
