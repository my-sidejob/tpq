@extends('layouts.app')

@section('title', 'Form jilid')

@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form jilid</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($jilid->id)) ? route('jilid.store') : route('jilid.update', $jilid->id) }}" method="post">
                    @csrf
                    @isset($jilid->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama jilid</label>
                        <input type="text" placeholder="Nama jilid" class="form-control" name="nama" value="{{ old('nama', $jilid->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" placeholder="Keterangan" class="form-control" name="keterangan" value="{{ old('keterangan', $jilid->keterangan) }}">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
