@extends('layouts.app')

@section('title', 'SPP')

@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data SPP</h4>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="row mb-5">
                        <div class="col-6">
                            <p>Dari</p>
                            <input type="text" name="dari" id="" class="form-control datepicker" required value="{{ $_GET['dari'] ?? '' }}">
                        </div>
                        <div class="col-6">
                            <p>Sampai</p>
                            <input type="text" name="sampai" id="" class="form-control datepicker" required value="{{ $_GET['sampai'] ?? '' }}">
                        </div>
                        <div class="col-12 mt-4">
                            <input type="submit" value="CARI" class="btn btn-block btn-primary">
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-12">
                        @if($type == 'default')
                            <a href="{{ route('spp.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah SPP</a>
                        @else
                            <a href="{{ route('pembayaran-spp.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah SPP</a>
                        @endif
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Santri</th>
                                    <th>Bulan</th>
                                    <th>Tahun</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                    <th>Bukti Pembayaran</th>
                                    @if($type == 'default')
                                    <th>Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($spp as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->santri->nama }}</td>
                                        <td>{{ $row->bulan }}</td>
                                        <td>{{ $row->tahun }}</td>
                                        <td>{{ 'Rp. ' . number_format($row->nominal) }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td><a href="{{ asset('img/bukti_pembayaran/'.$row->bukti_pembayaran) }}" target="_blank">Lihat</a></td>
                                        @if($type == 'default')
                                            <td>
                                                <form action="{{ route('spp.destroy', $row->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <a href="{{ route('spp.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">Belum ada data</td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
