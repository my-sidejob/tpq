@extends('layouts.app')

@section('title', 'Form spp')

@section('content')
<div class="row ">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form SPP</h4>
            </div>
            <div class="card-body">
                <form action="{{ Auth::guard('web')->check() ? (!isset($spp->id)) ? route('spp.store') : route('spp.update', $spp->id) : route('pembayaran-spp.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @isset($spp->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label for="">Santri</label>
                        @isset($spp->id)
                            <input type="text" name="santri_id" value="{{ $spp->santri_id }}" class="d-none">
                            <input type="text" name="" value="{{ $spp->santri->nama }}" readonly class="form-control">
                        @else
                            <select name="santri_id" class="form-control mySelect" id="santri_id">
                                <option value="">- Pilih Santri -</option>
                                @foreach($santri as $option)
                                    <option value="{{ $option->id }}" {{ old('santri_id', $spp->santri_id) == $option->id ? 'selected' : '' }} class="text-dark" data-santri_id="{{ $option->id }}">{{ $option->nama }}</option>
                                @endforeach
                            </select>
                        @endisset
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Bulan</label>
                                <select name="bulan" class="form-control mySelect" id="bulan_select">
                                    <option value="">- Pilih bulan -</option>
                                    @foreach(getBulan() as $key => $option)
                                        <option value="{{ $key }}" {{ old('bulan', $spp->bulan) == $key ? 'selected' : '' }} class="text-dark">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 d-none">
                            <div class="form-group">
                                <label>Tahun</label>
                                <input type="hidden" placeholder="Tahun" class="form-control" name="tahun" value="{{ now()->format('Y') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nominal</label>
                                <input type="number" placeholder="Nominal" class="form-control" name="nominal" value="{{ old('nominal', $spp->nominal) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Bukti Pembayaran</label>
                                @if(Auth::guard('web')->check())
                                    <br>
                                    @if(!empty($spp->bukti_pembayaran))
                                        <a href="{{ asset('img/bukti_pembayaran/'.$spp->bukti_pembayaran) }}" target="_blank">Lihat</a>
                                    @else
                                        <input type="file" placeholder="bukti_pembayaran" class="form-control" name="bukti_pembayaran" value="{{ old('bukti_pembayaran', $spp->bukti_pembayaran) }}">
                                    @endif
                                @else
                                    <input type="file" placeholder="bukti_pembayaran" class="form-control" name="bukti_pembayaran" value="{{ old('bukti_pembayaran', $spp->bukti_pembayaran) }}">
                                @endif
                            </div>
                        </div>
                    </div>

                    @if(Auth::guard('web')->check())
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control">
                                <option value="0" {{ old('status', $spp->status) == "0" ? 'selected' : '' }} class="text-dark">Pending</option>
                                <option value="1" {{ old('status', $spp->status) == "1" ? 'selected' : '' }} class="text-dark">Terima</option>
                                <option value="2" {{ old('status', $spp->status) == "2" ? 'selected' : '' }} class="text-dark">Tolak</option>
                            </select>
                        </div>
                    @else
                        <input type="hidden" name="status" class="form-control" readonly value="0">
                    @endif

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')

<script>
    $('#santri_id').change(function(){
        var santri_id = $(this).val();
        $.ajax({
            type:"GET",
            url: "{{ url('check-month-spp') }}" + '/' + santri_id,
            success: function(res){
                $('#bulan_select').html(res);
            }
        });
    });
</script>

@endpush
