<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                @if($detailProfile != null)
                    <img src="{{ asset('img/logo/'.$detailProfile->logo) }}" alt="person" class="img-fluid rounded-circle">
                @else
                    <img src="{{ asset('img/icon.jpg') }}" alt="person" class="img-fluid rounded-circle">
                @endif
                <h2 class="h5">{{ $detailProfile->nama }}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>S</strong><strong class="text-primary">I</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->

        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                @if(Auth::guard('web')->check())
                    <li class="{{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}"><a href="{{ url('dashboard') }}"> <i class="icon-home"></i>Dashboard</a></li>
                    @if(Auth::guard('web')->user()->level == "1")
                        <li class="{{ (urlHasPrefix('user') == true ) ? 'active' : '' }}"><a href="{{ url('user') }}"> <i class="fa fa-user-tie"></i>User</a></li>
                    @endif
                    <li class="{{ (urlHasPrefix('wali') == true ) ? 'active' : '' }}"><a href="{{ url('wali') }}"> <i class="fa fa-user"></i>Wali</a></li>
                    <li class="{{ (urlHasPrefix('santri') == true ) ? 'active' : '' }}"><a href="{{ url('santri') }}"> <i class="fa fa-users"></i>Santri</a></li>
                    <li class="{{ (urlHasPrefix('kelompok') == true ) ? 'active' : '' }}"><a href="{{ url('kelompok') }}"> <i class="fa fa-book-open"></i>Kelompok</a></li>
                    <li class="{{ (urlHasPrefix('kategori-pen') == true ) ? 'active' : '' }}"><a href="{{ url('kategori-penilaian') }}"> <i class="fa fa-check"></i>Kategori Penilaian</a></li>
                    <li class="{{ (urlHasPrefix('jadwal') == true ) ? 'active' : '' }}"><a href="{{ url('jadwal') }}"> <i class="fa fa-list"></i>Jadwal</a></li>
                    @if(Auth::guard('web')->user()->level == "1")
                       <li class="{{ (urlHasPrefix('profile') == true ) ? 'active' : '' }}"><a href="{{ url('profile') }}"> <i class="fa fa-school"></i>Profile</a></li>
                    @endif
                    <li class="{{ (urlHasPrefix('pengumuman') == true ) ? 'active' : '' }}"><a href="{{ url('pengumuman') }}"> <i class="fa fa-newspaper"></i>Pengumuman</a></li>
                    <li class="{{ (urlHasPrefix('spp') == true ) ? 'active' : '' }}"><a href="{{ url('spp') }}"> <i class="fa fa-receipt"></i>SPP</a></li>
                    @if(Auth::guard('web')->user()->level == "2")
                    <li class="{{ (urlHasPrefix('/nilai') == true ) ? 'active' : '' }}"><a href="{{ url('nilai') }}"> <i class="fa fa-clipboard-check"></i>Nilai</a></li>
                    <li class="{{ (urlHasPrefix('absensi') == true ) ? 'active' : '' }}"><a href="{{ url('absensi') }}"> <i class="fa fa-list-alt"></i>Absensi</a></li>
                    @endif
                @else
                    <li class="{{ (urlHasPrefix('dashboard-wali') == true ) ? 'active' : '' }}"><a href="{{ url('dashboard-wali') }}"> <i class="icon-home"></i>Dashboard</a></li>
                    <li class="{{ (urlHasPrefix('santri-wali') == true ) ? 'active' : '' }}"><a href="{{ url('santri-wali') }}"> <i class="fa fa-users"></i>Santri</a></li>
                    <li class="{{ (urlHasPrefix('lihat-jadwal') == true ) ? 'active' : '' }}"><a href="{{ url('lihat-jadwal') }}"> <i class="fa fa-list"></i>Lihat Jadwal</a></li>
                    <li class="{{ (urlHasPrefix('pembayaran-spp') == true ) ? 'active' : '' }}"><a href="{{ url('pembayaran-spp') }}"> <i class="fa fa-receipt"></i>Pembayaran SPP</a></li>
                    <li class="{{ (urlHasPrefix('lihat-absensi') == true ) ? 'active' : '' }}"><a href="{{ url('lihat-absensi') }}"> <i class="fa fa-list-alt"></i>Absensi</a></li>
                    <li class="{{ (urlHasPrefix('lihat-nilai') == true ) ? 'active' : '' }}"><a href="{{ url('lihat-nilai') }}"> <i class="fa fa-clipboard-check"></i>Nilai</a></li>
                    <li class="{{ (urlHasPrefix('lihat-pengumuman') == true ) ? 'active' : '' }}"><a href="{{ url('lihat-pengumuman') }}"> <i class="fa fa-newspaper"></i>Pengumuman</a></li>
                @endif
            </ul>
        </div>


    </div>
</nav>
