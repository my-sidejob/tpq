@extends('layouts.app')

@section('title', 'Form pengumuman')

@section('content')
<div class="row ">
    <div class="col-lg-7">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form pengumuman</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pengumuman->id)) ? route('pengumuman.store') : route('pengumuman.update', $pengumuman->id) }}" method="post">
                    @csrf
                    @isset($pengumuman->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama pengumuman</label>
                        <input type="text" placeholder="Nama pengumuman" class="form-control" name="nama_pengumuman" value="{{ old('nama_pengumuman', $pengumuman->nama_pengumuman) }}">
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" placeholder="Tanggal" class="form-control datepicker" name="tgl" value="{{ old('tgl', $pengumuman->tgl) }}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea placeholder="Keterangan" class="form-control" name="keterangan">{{ old('keterangan', $pengumuman->keterangan) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1" {{ old('status', $pengumuman->status) == "1" ? 'selected' : '' }} class="text-dark">Aktif</option>
                            <option value="0" {{ old('status', $pengumuman->status) == "0" ? 'selected' : '' }} class="text-dark">Tidak Aktif</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
