@extends('layouts.app')

@section('title', 'Detail Pengumuman')



@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>{{ $pengumuman->nama_pengumuman }}</h4>
            </div>
            <div class="card-body">
                <p>{{ $pengumuman->tgl }}</p>
                <pre style="font-family: 'Open Sans'; line-height:1.8em; color:#000;">{{ $pengumuman->keterangan }}</pre>
            </div>
        </div>
    </div>
</div>



@endsection
