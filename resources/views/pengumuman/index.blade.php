@extends('layouts.app')

@section('title', 'pengumuman')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data pengumuman</h4>
            </div>
            <div class="card-body">
                @if(Auth::guard('web')->check())
                <a href="{{ route('pengumuman.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah pengumuman</a>
                @else
                    <p>Pengumuman yang tersedia saat ini:</p>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama pengumuman</th>
                            <th>Tanggal</th>
                            <th>Status</th>

                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pengumuman as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_pengumuman }}</td>
                                <td>{{ $row->tgl }}</td>
                                <td>{!! $row->getStatus() !!}</td>
                                <td>
                                    @if(Auth::guard('web')->check())
                                    <form action="{{ route('pengumuman.destroy', $row->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('pengumuman.show', $row->id) }}" class="btn btn-sm btn-info text-white" title="Lihat Detail"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('pengumuman.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                        <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @else
                                        <a href="{{ route('detail.pengumuman', $row->id) }}" class="btn btn-sm btn-info text-white" title="Lihat Detail"><i class="fa fa-eye"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Belum ada data</td>
                            </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
