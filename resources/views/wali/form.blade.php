@extends('layouts.app')

@section('title', 'Form Wali')

@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Wali</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($wali->id)) ? route('wali.store') : route('wali.update', $wali->id) }}" method="post">
                    @csrf
                    @isset($wali->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" placeholder="Nama Wali" class="form-control" name="nama" value="{{ old('nama', $wali->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" placeholder="Alamat Email" class="form-control" name="email" value="{{ old('email', $wali->email) }}">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Password" class="form-control" name="password" value="">
                    </div>

                    <div class="form-group">
                        <label>No. Telp</label>
                        <input type="text" placeholder="No. Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $wali->no_telp) }}">
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Masukan alamat">{{ old('alamat', $wali->alamat) }}</textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
