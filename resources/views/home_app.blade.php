<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title') | {{ $detailProfile->nama ?? 'Default' }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="{{ asset('templates/vendor/font-awesome/css/all.min.css') }}">

        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{ asset('templates/css/style.default.css') }}" id="theme-stylesheet">
        <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    </head>
    <body>

        <div class="container-fluid border-bottom">
            <div class="container">
                <header class="d-flex flex-wrap justify-content-center py-3">
                    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                        <img src="{{ asset('img/logo/' . $detailProfile->logo) }}" alt="" width="40"> &nbsp;
                        <span class="fs-4">{{ $detailProfile->nama ?? 'Default' }}</span>
                    </a>

                    <ul class="nav nav-pills">
                        <li class="nav-item"><a href="{{ url('/') }}" class="nav-link text-primary" aria-current="page">Home</a></li>
                        <li class="nav-item"><a href="{{ url('tentang-kami') }}" class="nav-link text-primary" aria-current="page">Tentang Kami</a></li>
                        <li class="nav-item"><a href="{{ url('/#galeri') }}" class="nav-link text-primary">Galeri</a></li>
                        <li class="nav-item"><a href="{{ url('/#kontak') }}" class="nav-link text-primary">Kontak</a></li>
                        <li class="nav-item"><a href="{{ url('daftar') }}" class="nav-link text-primary">Daftar</a></li>
                        @if(Auth::guard('wali')->check())
                        <li class="nav-item"><a href="{{ url('dashboard-wali') }}" class="nav-link text-primary">Dashboard</a></li>
                            <form action="{{ (Auth::guard('wali')->check()) ? route('logout.wali') : route('logout') }}" method="post">
                                @csrf
                                <li class="nav-item"><button type="submit" class="nav-link text-primary"> <span class="d-none d-sm-inline-block"><i class="fa fa-sign-out-alt"></i> Logout</span></button></li>
                            </form>
                        @else
                            <li class="nav-item"><a href="{{ url('login-wali') }}" class="nav-link text-primary">Login</a></li>
                        @endif
                    </ul>
                </header>
            </div>
        </div>

        @yield('content')

        <div class="container-fluid bg-primary py-5 text-white" id="kontak">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div style="max-width:220px; margin:auto" class="mt-5">
                            <img src="{{ asset('img/logo/' . $detailProfile->logo) }}" alt="" class="img-fluid rounded">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h2 class="pb-3">Hubungi Kami</h2>
                        <hr>
                        <ul class="p-0 m-0 list" style="list-style: none">
                            <li class="bg-primary text-white border-0 py-2"><i class="fa fa-home"></i> {{ $detailProfile->alamat }}</li>
                            <li class="bg-primary text-white border-0 py-2"><i class="fa fa-phone"></i> {{ $detailProfile->no_telp }}</li>
                            <li class="bg-primary text-white border-0 py-2"><i class="fa fa-envelope"></i> {{ $detailProfile->email }}</li>
                        </ul>
                        <div class="mb-5"></div>
                    </div>
                    <div class="col-md-3">
                        <h2 class="pb-3">Tentang Kami</h2>
                        <hr>
                        <p>Taman Pendidikan Al-Qur'an bina insani merupakan lembaga pendidikan yang mengutaman pembelajaran Al-Qur'an dan Pendidikan Agama Islam. Dengan mengaplikasikan Al-Qur'an dan sunnah dalam pembelajaran dan kehidupan sehari-hari, sehingga menghasilkan generasi Qur'ani dan Berakhlakul Karimah.</p>
                    </div>
                    <div class="col-md-2">
                        <h2 class="pb-3">Tautan</h2>
                        <hr>
                        <ul>
                            <li class="nav-item"><a href="{{ url('/') }}" class="nav-link text-white" aria-current="page">Home</a></li>
                            <li class="nav-item"><a href="{{ url('tentang-kami') }}" class="nav-link text-white" aria-current="page">Tentang Kami</a></li>
                            <li class="nav-item"><a href="{{ url('/#galeri') }}" class="nav-link text-white">Galeri</a></li>
                            <li class="nav-item"><a href="{{ url('/#kontak') }}" class="nav-link text-white">Kontak</a></li>
                            <li class="nav-item"><a href="{{ url('daftar') }}" class="nav-link text-white">Daftar</a></li>
                            @if(!Auth::guard('wali')->check())
                                <li class="nav-item"><a href="{{ url('login-wali') }}" class="nav-link text-white">Login</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid text-white text-center" style="background:#239833">
            <div class="pt-3 pb-3">
                <p class="mb-0 text-sm">
                    &copy; {{ date('Y') }} - {{ $detailProfile->nama }}
                </p>
            </div>
        </div>

        <script src="{{ asset('templates/vendor/jquery/jquery.min.js') }}"></script>
        @stack('scripts')
    </body>
</html>
