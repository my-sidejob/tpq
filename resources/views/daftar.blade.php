@extends('home_app')

@section('title', 'Daftar')

@section('content')
<div class="container py-5">
    <div class="row py-5">
        <div class="col-12">

        </div>
        <div class="mb-4">
            <h1 class="mb-3">Daftar Santri Baru</h1>
            <p>Silahkan isi form dibawah untuk mendaftar.</p>
            @if(!Auth::guard('wali')->check())
            <p>Perlu diingat bahwa email dan password anda akan digunakan untuk login ke halaman wali. Jika sudah punya akun <a href="{{ url('login-wali') }}">klik disini</a> untuk login.</p>
            @endif
        </div>

        <div>
            @if($errors->any())
                <div class="mb-3">
                    <ul style="margin-bottom:0">
                    @foreach($errors->all() as $error)
                        <li class="text-danger">{{ $error }}</li>
                    @endforeach
                    </ul>

                </div>
            @endif
        </div>

        <form action="{{ route('daftar.store') }}" method="post">
            @csrf
            @isset($santri->id)
                {{ method_field('PUT')}}
            @endisset
            @if(!Auth::guard('wali')->check())
            <div class="form-group">
                <label>Nama Anda</label>
                <input type="text" placeholder="Nama Wali" class="form-control" name="nama" value="{{ old('nama', $wali->nama) }}">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" placeholder="Alamat Email" class="form-control" name="email" value="{{ old('email', $wali->email) }}">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" placeholder="Password" class="form-control" name="password" value="">
            </div>

            <div class="form-group">
                <label>No. Telp</label>
                <input type="text" placeholder="No. Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $wali->no_telp) }}">
            </div>

            <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat" class="form-control" placeholder="Masukan alamat">{{ old('alamat', $wali->alamat) }}</textarea>
            </div>
            @endif

            <div class="form-group">
                <label>Nama Santri</label>
                <input type="text" placeholder="Nama santri" class="form-control" name="nama_santri" value="{{ old('nama', $santri->nama) }}">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" placeholder="Tempat Lahir" class="form-control" name="tempat_lhr_santri" value="{{ old('tempat_lhr_santri', $santri->tempat_lhr) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="text" placeholder="Tanggal Lahir" class="form-control datepicker" name="tgl_lhr_santri" value="{{ old('tgl_lhr_santri', $santri->tgl_lhr) }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Jenis Kelamin</label><br>
                <label for="l">
                    <input type="radio" id="l" name="j_kel" value="l" {{ old('j_kel', $santri->j_kel) == 'l' ? 'checked' : '' }}> Laki - Laki
                </label> &nbsp;
                <label for="p">
                    <input type="radio" id="p" name="j_kel" value="p" {{ old('j_kel', $santri->j_kel) == 'p' ? 'checked' : '' }}> Perempuan
                </label>
            </div>

            @if(Auth::guard('web')->check())
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    <option value="1" {{ old('status', $santri->status) == "1" ? 'selected' : '' }} class="text-dark">Aktif</option>
                    <option value="0" {{ old('status', $santri->status) == "0" ? 'selected' : '' }} class="text-dark">Tidak Aktif</option>
                </select>
            </div>
            @else
                <input type="hidden" name="status" class="form-control" readonly value="0">
            @endif

            <div class="form-group" style="text-align: right">
                <input type="submit" class="btn btn-primary float-right" value="Daftar">
            </div>
        </form>
    </div>
</div>
@endsection
