@extends('layouts.app')

@section('title', 'Dashboard')

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.css">
@endsection

@section('content')
    <div class="row">
        @if(auth()->guard('web')->check())
            <div class="col-4">
                <div class="card income text-center py-3">
                    <div class="icon" style="font-size: 4rem"><i class="fa fa-users"></i></div>
                    <div class="number">{{ $total_santri }}</div><strong class="text-primary">Total Santri</strong>
                </div>
            </div>
            <div class="col-4">
                <div class="card income text-center py-3">
                    <div class="icon" style="font-size: 4rem"><i class="fa fa-user"></i></div>
                    <div class="number">{{ $total_wali }}</div><strong class="text-primary">Total Wali</strong>
                </div>
            </div>
            <div class="col-4">
                <div class="card income text-center py-3">
                    <div class="icon" style="font-size: 4rem"><i class="fa fa-user-tie"></i></div>
                    <div class="number">{{ $total_users }}</div><strong class="text-primary">Total Pengurus</strong>
                </div>
            </div>
        @endif

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Dashboard</h4>
                </div>
                <div class="card-body">
                    {{ ucwords("Selamat Datang ") . (Auth::guard('web')->check() == true ? '' : ucwords(Auth::user()->nama)) }} di sistem informasi {{ $detailProfile->nama }}
                </div>
            </div>
        </div>

        <div class="col-8">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Calendar</h4>
                </div>
                <div class="card-body">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    Daftar Pengumuman
                </div>
                <div class="card-body">
                    @forelse ($pengumuman as $p)
                        @if(auth()->guard('web') === true)
                            <p><a href="{{ route('pengumuman.show', $p->id) }}">{{ $p->nama_pengumuman }}</a> - <span class="badge badge-info">{{ $p->tgl }}</span></p>
                        @else 
                            <p><a href="{{ route('detail.pengumuman', $p->id) }}">{{ $p->nama_pengumuman }}</a> - <span class="badge badge-info">{{ $p->tgl }}</span></p>
                        @endif
                    @empty 
                        <p>Tidak ada pengumuman terbaru</p>
                    @endforelse
                </div>
            </div>
        </div>

        
    </div>



@endsection


@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js"></script>
<script>

    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth'
      });
      calendar.render();
    });

</script>
<script>
    $('.active')[1].classList.remove("active");
</script>

@endpush
