@extends('layouts.app')

@section('title', 'Admin, Ustadz & Ustadzah')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Admin, Ustadz & Ustadzah</h4>
            </div>
            <div class="card-body">
            <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Admin, Ustadz & Ustadzah</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{!! $row->getLevel() !!}</td>
                                <td>{!! $row->getStatus() !!}</td>
                                <td>
                                    <form action="{{ route('user.destroy', $row->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('user.show', $row->id) }}" class="btn btn-sm btn-info" title="Lihat Detail"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('user.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                        <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Belum ada data</td>
                            </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
