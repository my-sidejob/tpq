@extends('layouts.app')

@section('title', 'Form Admin, Ustadz, Ustadzah')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Admin, Ustadz, Ustadzah</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($user->id)) ? route('user.store') : route('user.update', $user->id) }}" method="post">
                    @csrf
                    @isset($user->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" placeholder="Nama Admin / Ustadz / Ustadzah" class="form-control" name="nama" value="{{ old('nama', $user->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" placeholder="Alamat Email" class="form-control" name="email" value="{{ old('email', $user->email) }}">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Password" class="form-control" name="password" value="">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" placeholder="Tempat Lahir" class="form-control" name="tempat_lhr" value="{{ old('tempat_lhr', $user->tempat_lhr) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="text" placeholder="Tanggal Lahir" class="form-control datepicker2" name="tgl_lhr" value="{{ old('tgl_lhr', $user->tgl_lhr) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jenis Kelamin</label><br>
                                <label for="l">
                                    <input type="radio" id="l" name="j_kel" value="l" {{ old('j_kel', $user->j_kel) == 'l' ? 'checked' : '' }}> Laki - Laki
                                </label> &nbsp;
                                <label for="p">
                                    <input type="radio" id="p" name="j_kel" value="p" {{ old('j_kel', $user->j_kel) == 'p' ? 'checked' : '' }}> Perempuan
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>No. Telp</label>
                                <input type="text" placeholder="No. Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $user->no_telp) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Masukan alamat">{{ old('alamat', $user->alamat) }}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Level</label>
                                <select name="level" class="form-control">
                                    <option value="1" {{ old('level', $user->level) == '1' ? 'selected' : '' }} class="text-dark">Admin</option>
                                    <option value="2" {{ old('level', $user->level) == '2' ? 'selected' : '' }} class="text-dark">Ustadz / Ustadzah</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="1" {{ old('status', $user->status) == "1" ? 'selected' : '' }} class="text-dark">Aktif</option>
                                    <option value="0" {{ old('status', $user->status) == "0" ? 'selected' : '' }} class="text-dark">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
