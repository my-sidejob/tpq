@extends('layouts.app')

@section('title', 'Detail Admin, Ustadz & Ustadzah')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Detail Admin, Ustadz & Ustadzah</h4>
            </div>
            <div class="card-body">
                <table class="table">

                    <tr>
                        <td style="border-top: 0">Nama</td>
                        <td  style="border-top: 0">{{ $user->nama }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>TTL</td>
                        <td>{{ $user->tempat_lhr . ', ' . $user->formatDate() }}</td>
                    </tr>
                    <tr>
                        <td>No. Telp</td>
                        <td>{{ $user->no_telp }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>{{ $user->alamat }}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>{{ strtoupper($user->j_kel) }}</td>
                    </tr>
                    <tr>
                        <td>Level</td>
                        <td>{!! $user->getLevel() !!}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>{!! $user->getStatus() !!}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
