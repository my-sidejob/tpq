@extends('home_app')

@section('title', 'Selamat Datang')

@section('content')

<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('img/slider/slider_new.png') }}" class="d-block w-100" alt="slide">
            {{-- <div class="carousel-caption d-none d-md-block">
                <h5>First slide label</h5>
                <p>Some representative placeholder content for the first slide.</p>
            </div> --}}
        </div>
        <div class="carousel-item">
            <img src="{{ asset('img/slider/2.jpg') }}" class="d-block w-100" alt="slide">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('img/slider/1.jpg') }}" class="d-block w-100" alt="slide">
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>

    <div class="container col-xxl-8 px-4 py-5" id="home">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
            <div class="col-md-6 col-lg-6">
                <img src="{{ asset('img/banner.jpg') }}" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
            </div>
            <div class="col-md-6 col-lg-6">
                <h1 class="display-5 fw-bold lh-1 mb-3">Selamat Datang</h1>
                <p class="lead">Taman Pendidikan Al-Qur'an bina insani merupakan lembaga pendidikan yang mengutaman pembelajaran Al-Qur'an dan Pendidikan Agama Islam. Dengan mengaplikasikan Al-Qur'an dan sunnah dalam pembelajaran dan kehidupan sehari-hari, sehingga menghasilkan generasi Qur'ani dan Berakhlakul Karimah</p>
                <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                    <a href="{{ url('daftar') }}" class="btn btn-primary btn-lg px-4 me-md-2">Daftar</a>
                    <a href="{{ url('login-wali') }}" class="btn btn-outline-secondary btn-lg px-4">Login</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container col-xxl-8 px-4 py-5" id="home">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
            <div class="col-md-6 col-lg-6">
                <h1 class="display-5 fw-bold lh-1 mb-3">Tentang Kami</h1>
                <p class="lead"> TPQ Bina Insani adalah pendidikan non formal yang menerapkan kurikulum BKPAKSI. Hingga saat ini TPQ Bina Insani telah menghasilakan peserta didik (santri) yang mampu membaca Al-Qur'an dengan fasih.</p>
                <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                    <a href="{{ url('tentang-kami') }}" class="btn btn-outline-primary btn-lg px-4">Selengkapnya</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <img src="{{ asset('img/section.jpg') }}" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
            </div>
        </div>
    </div>

    <div class="container col-xxl-8 px-4 py-5" id="">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
            <div class="col-12 text-center">
                <div class="mb-5">
                    <h1 class="">Motivasi Hari Ini</h1>
                </div>

                <p>“Barang siapa yang memudah kesulitan seorang mu’min dari berbagai kesulitan-kesulitan dunia, Allah akan memudahkan kesulitan-kesulitannya pada hari kiamat. Dan siapa yang memudahkan orang yang sedang dalam kesulitan niscaya akan Allah memudahkan baginya di dunia dan akhirat” <br> (HR. Muslim).</p>
            </div>
        </div>
    </div>

    <div class="container px-4 py-5" id="galeri">
        <h1 class="pb-2 mb-3">Galeri Kami</h1>
        <div class="row">
            <div class="col-12">
                <div class="galery flex-images">
                    <div class="item" data-w="480" data-h="724">
                        <img src="{{ asset('img/galeri/Image01.jpeg') }}" alt="">
                    </div>
                    <div class="item" data-w="640" data-h="460">
                        <img src="{{ asset('img/galeri/Image02.jpeg') }}" alt="">
                    </div>
                    <div class="item" data-w="745" data-h="1090">
                        <img src="{{ asset('img/galeri/Image04.jpeg') }}" alt="">
                    </div>
                    <div class="item" data-w="1280" data-h="606">
                        <img src="{{ asset('img/galeri/Image06.jpeg') }}" alt="">
                    </div>
                    <div class="item" data-w="1040" data-h="492">
                        <img src="{{ asset('img/galeri/Image08.jpeg') }}" alt="">
                    </div>
                    <div class="item" data-w="1280" data-h="835">
                        <img src="{{ asset('img/galeri/Image10.jpeg') }}" alt="">
                    </div>
                    <div class="item" data-w="1600" data-h="1200">
                        <img src="{{ asset('img/galeri/Image11.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/jquery.flex-images.min.js') }}"></script>

<script type="text/javascript">
    $(".galery").flexImages({rowHeight:500});
    var myCarousel = document.querySelector('#myCarousel')
    var carousel = new bootstrap.Carousel(myCarousel, {
        interval: 2000,
        wrap: false
    })
</script>

@endpush
