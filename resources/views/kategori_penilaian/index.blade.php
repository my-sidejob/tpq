@extends('layouts.app')

@section('title', 'Kategori Penilaian')

@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Kategori Penilaian</h4>
            </div>
            <div class="card-body">
            <a href="{{ route('kategori-penilaian.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah kategori penilaian</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($kategori_penilaian as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->keterangan }}</td>
                                <td>
                                    <form action="{{ route('kategori-penilaian.destroy', $row->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('kategori-penilaian.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                        <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Belum ada data</td>
                            </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
