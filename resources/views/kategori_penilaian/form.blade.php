@extends('layouts.app')

@section('title', 'Form Kategori Penilaian')

@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Kategori Penilaian</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($kategori_penilaian->id)) ? route('kategori-penilaian.store') : route('kategori-penilaian.update', $kategori_penilaian->id) }}" method="post">
                    @csrf
                    @isset($kategori_penilaian->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama kategori penilaian</label>
                        <input type="text" placeholder="Nama kategori penilaian" class="form-control" name="nama" value="{{ old('nama', $kategori_penilaian->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" placeholder="Keterangan" class="form-control" name="keterangan" value="{{ old('keterangan', $kategori_penilaian->keterangan) }}">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
