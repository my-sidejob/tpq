@extends('home_app')

@section('title', 'Tentang Kami')

@section('content')
    <div class="container">
        <img src="{{ asset('img/slider/3.jpg') }}" class="d-block w-100" alt="image">
    </div>

    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12">
                <h1 class="display-5 fw-bold lh-1 mb-3">Tentang Kami</h1>
                <p>
                    Taman Pendidikan Al-Qur'an Bina Insani merupakan lembaga pendidikan yang mengutamakan pembelajaran Al-Qur'an dan Pendidikan Agama Islam. Dengan mengaplikasikan Al-Qur'an dan sunnah dalam pembelajaran dan kehidupan sehari-hari, sehingga menghasilkan generasi Qur'ani dan Berakhlakul Karimah. Taman Pendidikan Al-Qur'an (TPQ) Bina Insani didirikan pada tahun 1995 seiring dengan keinginan para orang tua yang memerlukan pendidikan agama Islam bagi putra-putrinya di dalam lingkungan masyarakat. TPQ Bina Insani adalah pendidikan non formal yang menerapkan kurikulum BKPAKSI. Hingga saat ini TPQ Bina Insani telah menghasilkan peserta didik (santri) yang mampu membaca Al-Qur'an dengan fasih.
                </p>
                <p>TPQ Bina Insani memiliki ciri khas dalam penguasaan membaca Al-Quran secara tartil dan penguasaan materi materi ibadah dan fiqih seperti sholat, doa-doa harian, surat -surat pendek dan bacaan Al Qur'an yang baik dan benar sesuai dengan ilmu tajwid. Dalam proses pembelajaran terhadap santri, TPQ bina insani berkomitmen untuk memberikan fasilitas belajar yang nyaman untuk menunjang pembelajaran yang baik</p>
            </div>
        </div>
        <div class="row pt-5 pb-5 mb-5">
            <div class="col-md-6">
                <img src="{{ asset('img/section2.jpg') }}" class="d-block w-100" alt="image">
            </div>
            <div class="col-md-6">
                <h1 class="display-5 fw-bold lh-1 mb-4">Visi Kami</h1>
                <p>
                    Mewujudkan lembaga pendidikan Islam yang unggul dan kompetitif hingga dapat melahirkan generasi Muslim & Muslimah yang memiliki kemampuan memikul amanah dengan baik sesuai ajaranNYA.
                </p>
                <hr class="mt-5 mb-5">
                <h1 class="display-5 fw-bold lh-1 mb-4">Misi Kami</h1>
                <p>
                    <ul>
                        <li>Memberikan wadah pendidikan yang berbasis islami.</li>
                        <li>Menyelenggarakan kegiatan belajar membaca dan menghafal Al-Qur'an.</li>
                        <li>Meningkatkan kompetensi santri dan pendidik dibidang pelajaran agama.</li>
                    </ul>
                </p>
                <hr class="mt-5 mb-5">
                <h1 class="display-5 fw-bold lh-1 mb-4">Tujuan Kami</h1>
                <p>
                    <ul>
                        <li>Mampu membaca Al-Qur'an dengan baik sesuai dengan kaidah Ilmu Tajwid.</li>
                        <li>Mampu mengembangkan potensi peserta didik (santri) ke arah pembinaan sikap, pengetahuan dan keterampilan agama.</li>
                        <li>Mempersiapkan peserta didik (santri) agar mampu mengembangkan sikap dan keterampilan keagamaan yang telah dimilikinya melalui program pendidikan lanjutan.</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
@endpush
