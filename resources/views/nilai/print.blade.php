<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Nilai</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
        .text-center {
            text-align: center;
        }
        .header p, .header h2 {
            margin-bottom: 0;
            margin-top: 0;
            line-height: 1.5em;
        }
        .table {
            width: 100%;
            text-align: left;
            border-spacing: 0;
        }
        .table th, .table td {
            border-bottom: 1px solid black;
            box-sizing: border-box;
            padding: 6px 10px;
            text-align: left;
        }
    </style>
</head>
<body>
    @if(!empty($detailProfile))
        <div class="text-center header">
            <div style="margin-bottom:10px">
                <img src="{{ public_path('img/logo/'.$detailProfile->logo) }}" alt="logo" width="80">
            </div>
            <h2>{{ $detailProfile->nama }}</h2>
            <p>{{ $detailProfile->alamat }}</p>
            <p>E: {{ $detailProfile->email }}  &nbsp; P: {{ $detailProfile->no_telp }}</p>
        </div>
    @else
        <p>Default Text here</p>
    @endif
    <hr>
    <p>Data nilai santri dibulan {{ getBulan()[$nilai['bulan']] }} tahun {{ $nilai['tahun'] }}</p>
    <table>
        <tr>
            <td style="width:140px">Nama</td>
            <td style="width:15px">:</td>
            <td>{{ $nilai->santri->nama }}</td>
        </tr>
        <tr>
            <td>Kelompok</td>
            <td>:</td>
            <td>{{ $nilai->kelompok->nama }}</td>
        </tr>
        <tr>
            <td>Bulan</td>
            <td>:</td>
            <td>{{ getBulan()[$nilai->bulan] }}</td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>:</td>
            <td>{{ $nilai->tahun }}</td>
        </tr>
    </table>
    <br>
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Kategori Penilaian</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <tbody>
            @foreach($detail_nilai as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->kategoriPenilaian->nama }}</td>
                    <td>{{ $row->nilai }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
