@extends('layouts.app')

@section('title', 'nilai')

@section('content')

    <div class="row">
        @if(auth()->guard('web')->check())
            <div class="col-12 mb-4">
                <a href="{{ route('nilai.create') }}" class="btn btn-primary">Tambah Nilai</a>
            </div>
        @endif
        <div class="col-md-6">
            <div class="card pb-3">
                <div class="card-header">
                    <h4>Pilih bulan dan tahun</h4>
                </div>
                <div class="card-body">
                    <form action="">
                        <div class="row">
                            <div class="col">
                                <label for="">Bulan</label>
                                <select name="bulan" class="form-control mySelect" id="bulan_select">
                                    <option value="">- Pilih bulan -</option>
                                    @foreach(getBulan() as $key => $option)
                                        <option value="{{ $key }}" {{ old('bulan') == $key ? 'selected' : '' }} class="text-dark">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <label for="">Tahun</label>
                                <input type="text" name="tahun" class="form-control" value="{{ now()->format('Y') }}">
                            </div>
                        </div>

                        <div class="form-group mt-4">
                            <input type="submit" class="btn btn-primary float-right" value="Cari">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @if($nilai !== null)
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>Data nilai</h4>
                    </div>
                    <div class="card-body">
                        <p>Data nilai santri dibulan {{ $_GET['bulan'] }} tahun {{ $_GET['tahun'] }}</p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Kelompok</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($nilai as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->santri->nama }}</td>
                                        <td>{{ $row->kelompok->nama }}</td>
                                        <td>
                                            @if(auth()->guard('web')->check())
                                            <form action="{{ route('nilai.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ route('nilai.show', $row->id) }}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                                <a href="{{ route('nilai.edit', $row->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-cog"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                                <a href="{{ route('nilai.print', $row->id) }}" target="_blank" class="btn btn-sm btn-secondary"><i class="fa fa-print"></i></a>
                                            </form>
                                            @else
                                                <a href="{{ route('lihat.nilai.detail', $row->id) }}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                                <a href="{{ route('nilai.cetak', $row->id) }}" target="_blank" class="btn btn-sm btn-secondary"><i class="fa fa-print"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>Tidak ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    @endif



@endsection
