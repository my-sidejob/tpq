@extends('layouts.app')

@section('title', 'Detail Nilai')

@section('content')
<div class="row">
    <div class="col-6 mb-4">
        <div class="card">
            <div class="card-header">
                <h4>Detail Nilai</h4>
            </div>
            <div class="card-body">
                <p>Detail nilai santri</p>
                <table class="table">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ $nilai->santri->nama }}</td>
                    </tr>
                    <tr>
                        <td>Kelompok</td>
                        <td>:</td>
                        <td>{{ $nilai->kelompok->nama }}</td>
                    </tr>
                    <tr>
                        <td>Bulan</td>
                        <td>:</td>
                        <td>{{ $nilai->bulan }}</td>
                    </tr>
                    <tr>
                        <td>Tahun</td>
                        <td>:</td>
                        <td>{{ $nilai->tahun }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kategori Penilaian</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($detail_nilai as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->kategoriPenilaian->nama }}</td>
                                <td>{{ $row->nilai }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
