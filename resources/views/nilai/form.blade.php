@extends('layouts.app')

@section('title', 'Form nilai')

@section('content')
<form action="{{ (!isset($nilai->id)) ? route('nilai.store') : route('nilai.update', $nilai->id) }}" method="post">
    <div class="row ">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Form nilai</h4>
                </div>
                <div class="card-body">
                    @csrf
                    @isset($nilai->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label for="">Santri</label>
                        <select name="santri_id" class="form-control mySelect">
                            <option value="">- Pilih Santri -</option>
                            @foreach($santri as $option)
                                <option value="{{ $option->id }}" {{ old('santri_id', $nilai->santri_id) == $option->id ? 'selected' : '' }} class="text-dark">{{ $option->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Kelompok</label>
                        <select name="kelompok_id" class="form-control mySelect">
                            <option value="">- Pilih kelompok -</option>
                            @foreach($kelompok as $option)
                                <option value="{{ $option->id }}" {{ old('kelompok_id', $nilai->kelompok_id) == $option->id ? 'selected' : '' }} class="text-dark">{{ $option->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Bulan</label>
                        <select name="bulan" class="form-control mySelect" id="bulan_select">
                            <option value="">- Pilih bulan -</option>
                            @foreach(getBulan() as $key => $option)
                                <option value="{{ $key }}" {{ old('bulan', $nilai->bulan) == $key ? 'selected' : '' }} class="text-dark">{{ $option }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tahun</label>
                        <input type="text" name="tahun" class="form-control" value="{{ now()->format('Y') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    <h4>Kategori Penilaian</h4>
                </div>
                <div class="card-body">
                    <div id="penilaian">
                        @if(!isset($nilai->id))
                            <div class="row row-penilaian">
                                <div class="col-8">
                                    <div class="form-group">
                                        <label for="">Kategori Penilaian</label>
                                        <select name="kategori_penilaian_id[]" class="form-control mySelect" required>
                                            <option value="">- Pilih -</option>
                                            @foreach($kategori_penilaian as $key => $option)
                                                <option value="{{ $option->id }}" class="text-dark">{{ $option->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="">Nilai</label>
                                        <input type="text" name="nilai[]" class="form-control" value="" required>
                                    </div>
                                </div>
                                <div class="col-1">
                                    <div style="margin-top:2rem">
                                        <a href="#" class="btn btn-sm btn-danger disabled" disabled><i class="fa fa-minus"></i></a>
                                    </div>
                                </div>
                            </div>
                        @else
                            @foreach($detail_nilai as $index => $column)
                                <div class="row row-penilaian">
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label for="">Kategori Penilaian</label>
                                            <select name="kategori_penilaian_id[]" class="form-control mySelect" required>
                                                <option value="">- Pilih -</option>
                                                @foreach($kategori_penilaian as $key => $option)
                                                    <option value="{{ $option->id }}" {{ $column->kategori_penilaian_id == $option->id ? 'selected' : '' }} class="text-dark">{{ $option->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="">Nilai</label>
                                            <input type="text" name="nilai[]" class="form-control" value="{{ $column->nilai }}" required>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div style="margin-top:2rem">
                                            @if($index === 0)
                                                <a href="#" class="btn btn-sm btn-danger disabled"><i class="fa fa-minus"></i></a>
                                            @else
                                                <a href="#" class="btn btn-sm btn-danger row-delete" onclick="event.preventDefault(); rowDelete(this)"><i class="fa fa-minus"></i></a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <a href="#" id="add-column" class="text-info"><i class="fa fa-plus"></i> Tambah Kolom</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="form-group">
                <input type="submit" class="btn btn-primary float-right" value="Simpan">
            </div>
        </div>
    </div>
</form>

@endsection

@push('scripts')

<script>
    var html = `
        <div class="row row-penilaian">
            <div class="col-8">
                <div class="form-group">
                    <label for="">Kategori Penilaian</label>
                    <select name="kategori_penilaian_id[]" class="form-control mySelect" required>
                        <option value="">- Pilih -</option>
                        @foreach($kategori_penilaian as $key => $option)
                            <option value="{{ $option->id }}" class="text-dark">{{ $option->nama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="">Nilai</label>
                    <input type="text" name="nilai[]" class="form-control" value="" required>
                </div>
            </div>
            <div class="col-1">
                <div style="margin-top:2rem">
                    <a href="#" class="btn btn-sm btn-danger row-delete" onclick="event.preventDefault(); rowDelete(this)"><i class="fa fa-minus"></i></a>
                </div>
            </div>
        </div>
    `;
    $('#add-column').click(function(e){
        e.preventDefault();
        //select2 input option
        $('#penilaian').append(html);
        $('.mySelect').select2();
    });

    function rowDelete(el){
        //console.log(el);
        el.parentElement.parentElement.parentElement.parentElement.removeChild(el.parentElement.parentElement.parentElement)
    }


</script>

@endpush
