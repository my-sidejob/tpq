@extends('layouts.app')

@section('title', 'Form kelompok')

@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form kelompok</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($kelompok->id)) ? route('kelompok.store') : route('kelompok.update', $kelompok->id) }}" method="post">
                    @csrf
                    @isset($kelompok->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama kelompok</label>
                        <input type="text" placeholder="Nama kelompok" class="form-control" name="nama" value="{{ old('nama', $kelompok->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" placeholder="Keterangan" class="form-control" name="keterangan" value="{{ old('keterangan', $kelompok->keterangan) }}">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
