<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = [
        'nama',
        'alamat',
        'email',
        'no_telp',
        'deskripsi',
        'logo',
        'user_id',
    ];

    public static function getDefaultValues() {
        return (object) [
            'nama' => '',
            'alamat' => '',
            'email' => '',
            'no_telp' => '',
            'deskripsi' => '',
            'logo' => '',
        ];
    }
}
