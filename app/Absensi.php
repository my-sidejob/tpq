<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = 'absensi';
    protected $fillable = [
        'santri_id',
        'tgl',
        'status', //0 = alpha, 1 = hadir, 2 = izin
        'keterangan',
    ];

    public static function getDefaultValues() {
        return (object) [
            'santri_id' => '',
            'tgl' => date('Y-m-d'),
            'status' => '',
            'keterangan' => '',
        ];
    }

    public function getStatusAttribute($value) {
        return strval($value);
    }

    public function santri() {
        return $this->belongsTo('App\Santri');
    }

    public function getAbsenStatus() {
        if ($this->status == "0") {
            return '<span class="badge badge-danger">Alpha</span>';
        } else if($this->status == "1") {
            return '<span class="badge badge-success">Hadir</span>';
        } else if($this->status == "2") {
            return '<span class="badge badge-warning">Izin</span>';
        }
    }
}
