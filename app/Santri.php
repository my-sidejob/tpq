<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Santri extends Model
{
    protected $table = 'santri';
    protected $fillable = [
        'wali_id',
        'nama',
        'tempat_lhr',
        'tgl_lhr',
        'status',
        'j_kel',
    ];

    public static function getDefaultValues() {
        return (object) [
            'wali_id' => '',
            'nama' => '',
            'tempat_lhr' => '',
            'tgl_lhr' => '',
            'status' => '',
            'j_kel' => '',
        ];
    }

    public function getStatusAttribute($value) {
        return strval($value);
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Pending</span>';
        }
    }

    public function wali() {
        return $this->belongsTo('App\Wali');
    }

    public function formatDate() {
        $date = explode('-', $this->tgl_lhr);
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }

    public function getNilai($kelompok_id) {
        return Nilai::where('santri_id', $this->id)->where('kelompok_id', $kelompok_id)->first();
    }

}
