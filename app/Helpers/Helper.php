<?php

use App\Jadwal;
use App\Kelompok;

function urlHasPrefix(string $prefix) {
    $url = url()->current();
    if (strpos($url, $prefix) > 0) {
        return true;
    }

    return false;
}


function generateBreadcrumb(){

    $crumbs = explode("/",$_SERVER["REQUEST_URI"]);
    //dd((is_numeric($crumbs[2]) == true) ? 'benar' : 'salah');
    foreach($crumbs as $key=>$crumb){

        if($key == 0 || is_numeric($crumbs[$key])) {
            continue;
        } else {
            if($key == count($crumbs) - 1) {
                $breadcrumb = $crumbs[$key];
                if(strpos($breadcrumb, '?') !== false){
                    $breadcrumb = explode("?", $breadcrumb);
                    $crumb = $breadcrumb[0];
                }

                echo  '<li class="breadcrumb-item active">' . ucfirst(str_replace('-', ' ',$crumb) . ' ') . '</li>';
            } else {
                echo '<li class="breadcrumb-item"><a href="'.url($crumb).'">' . ucfirst(str_replace('-', ' ',$crumb) . ' ') . '</a></li>';
            }

        }

    }
}

function getHari() {
    return [
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jumat',
        'Sabtu',
        'Minggu',
    ];
}

function getJadwal($hari) {
    return Jadwal::where('hari', $hari)->orderBy("jam_mulai", 'asc')->get();
}

function getBulan() {
    return [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    ];
}

function getBulanShort() {
    return [
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'Mei',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Agu',
        '09' => 'Sep',
        '10' => 'Okt',
        '11' => 'Nov',
        '12' => 'Des',
    ];
}

function getKelompok(){
    return Kelompok::orderBy('nama', 'asc')->get();
}

?>
