<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    protected $table = 'kelompok';
    protected $fillable = [
        'nama',
        'keterangan',
    ];

    public static function getDefaultValues() {
        return (object) [
            'nama' => '',
            'keterangan' => '',
        ];
    }
}
