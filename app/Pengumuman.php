<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $table = 'pengumuman';
    protected $fillable = [
        'nama_pengumuman',
        'keterangan',
        'tgl',
        'status'
    ];

    public static function getDefaultValues() {
        return (object) [
            'nama_pengumuman' => '',
            'keterangan' => '',
            'tgl' => '',
            'status' => ''
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-secondary">Tidak Aktif</span>';
        }
    }

    public function getStatusAttribute($value) {
        return strval($value);
    }
}
