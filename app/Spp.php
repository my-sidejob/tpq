<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spp extends Model
{
    protected $table = 'spp';
    protected $fillable = [
        'santri_id',
        'bulan',
        'tahun',
        'nominal',
        'status',
        'bukti_pembayaran',
        'user_id',
    ];

    public static function getDefaultValues() {
        return (object) [
            'santri_id' => '',
            'bulan' => '',
            'tahun' => date('Y'),
            'nominal' => '',
            'status' => '',
            'bukti_pembayaran' => '',
            'user_id',
        ];
    }

    public function getStatusAttribute($value) {
        return strval($value);
    }

    public function santri() {
        return $this->belongsTo('App\Santri');
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Diterima</span>';
        } else if($this->status == "0") {
            return '<span class="badge badge-warning">Pending</span>';
        } else {
            return '<span class="badge badge-danger">Ditolak</span>';
        }
    }

}
