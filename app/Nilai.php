<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilai';
    protected $fillable = [
        'santri_id',
        'kelompok_id',
        'bulan',
        'tahun',

    ];

    public static function getDefaultValues() {
        return (object) [
            'santri_id' => '',
            'kelompok_id' => '',
            'bulan' => '',
            'tahun' => '',
        ];
    }

    public function santri()
    {
        return $this->belongsTo('App\Santri');
    }

    public function kelompok()
    {
        return $this->belongsTo('App\Kelompok');
    }

    public function detailNilai()
    {
        return $this->belongsToMany('App\DetailNilai', 'detail_nilai', 'nilai_id', 'kategori_penilaian_id');
    }

    public function rataRataNilai()
    {
        return round(DetailNilai::where('nilai_id', $this->id)->avg('nilai'));
    }

    public function convertBulan() {
        return getBulanShort()[$this->bulan];
    }
}
