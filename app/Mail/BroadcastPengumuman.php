<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BroadcastPengumuman extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $pengumuman;

    public function __construct($pengumuman)
    {
        $this->pengumuman = $pengumuman;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@tpqbinainsani.com')
        ->subject('[Pengumuman] - ' . $this->pengumuman->nama_pengumuman . ' - ' . $this->pengumuman->tgl)
        ->view('pengumuman.email', ['pengumuman' => $this->pengumuman]);
    }
}
