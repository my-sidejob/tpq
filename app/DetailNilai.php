<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailNilai extends Model
{
    protected $table = 'detail_nilai';
    protected $fillable = [
        'nilai_id',
        'kategori_penilaian_id',
        'nilai',
    ];

    public function nilai()
    {
        return $this->belongsTo('App\Nilai');
    }

    public function kategoriPenilaian(){
        return $this->belongsTo('App\KategoriPenilaian');
    }
}
