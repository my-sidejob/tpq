<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jilid extends Model
{
    protected $table = 'jilid';
    protected $fillable = [
        'nama',
        'keterangan',
    ];

    public static function getDefaultValues() {
        return (object) [
            'nama' => '',
            'keterangan' => '',
        ];
    }
}
