<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Wali extends Authenticatable
{
    use Notifiable;

    protected $table = 'wali';

    protected $fillable = [
        'nama',
        'email',
        'password',
        'alamat',
        'no_telp',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama' => '',
            'email' => '',
            'password' => '',
            'alamat' => '',
            'no_telp' => '',
        ];
    }

    public function getSantri() {
        return $this->hasMany('App\Santri');
    }
}
