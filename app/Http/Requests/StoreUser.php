<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->user);

        switch($this->method()){
            case 'POST':
            {
                return [
                    'email' => 'required|unique:users,email',
                    'nama' => 'required',
                    'password' => 'required',
                    'nama' => 'required',
                    'j_kel' => 'required',
                    'tempat_lhr' => 'required',
                    'tgl_lhr' => 'required',
                    'alamat' => 'required',
                    'no_telp' => 'required',
                    'level' => 'required',
                    'status' => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'email' => 'required|unique:users,email,' . $user->id,
                    'nama' => 'required',
                    'j_kel' => 'required',
                    'tempat_lhr' => 'required',
                    'tgl_lhr' => 'required',
                    'alamat' => 'required',
                    'no_telp' => 'required',
                    'level' => 'required',
                    'status' => 'required',
                ];
            }
        }
    }
}
