<?php

namespace App\Http\Controllers;

use App\Jilid;
use Illuminate\Http\Request;

class JilidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('jilid.index', ['jilid' => Jilid::orderBy('nama', 'asc')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['jilid'] = Jilid::getDefaultValues();

        return view('jilid.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        Jilid::create($request->toArray());
        return redirect()->route('jilid.index')->with('success', 'Berhasil menambah data jilid');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jilid'] = Jilid::findOrFail($id);
        return view('jilid.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        Jilid::findOrFail($id)->update($request->toArray());
        return redirect()->route('jilid.index')->with('success', 'Berhasil mengubah data jilid');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jilid::findOrFail($id)->delete();
        return redirect()->route('jilid.index')->with('success', 'Berhasil menghapus data jilid');
    }
}
