<?php

namespace App\Http\Controllers;

use App\Pengumuman;
use App\Santri;
use App\User;
use App\Wali;

class DashboardController extends Controller
{
    public function index() {
        $data['total_santri'] = Santri::where('status', '1')->get()->count();
        $data['total_wali'] = Wali::all()->count();
        $data['total_users'] = User::all()->count();
        $data['pengumuman'] = Pengumuman::orderBy('tgl')->where('status', '1')->limit(3)->get();
        return view('dashboard', $data);
    }

    public function indexWali() {
        $data['pengumuman'] = Pengumuman::orderBy('tgl')->where('status', '1')->limit(3)->get();
        return view('dashboard', $data);
    }
}
