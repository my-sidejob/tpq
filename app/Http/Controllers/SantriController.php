<?php

namespace App\Http\Controllers;

use App\Absensi;
use App\Nilai;
use App\Santri;
use App\Wali;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SantriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('web')->check()) {
            $santri =  Santri::orderBy('nama', 'asc')->get();
            $type = 'default';
        } else {
            $santri = Santri::where('wali_id', Auth::guard('wali')->user()->id)->orderBy('nama', 'asc')->get();
            $type = 'wali';
        }
        return view ('santri.index', ['santri' => $santri, 'type' => $type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['santri'] = Santri::getDefaultValues();
        $data['wali'] = Wali::orderBy('nama', 'asc')->get();

        return view('santri.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'wali_id' => 'required',
            'nama' => 'required',
            'tempat_lhr' => 'required',
            'tgl_lhr' => 'required',
            'j_kel' => 'required',
        ]);

        Santri::create($request->toArray());
        if(Auth::guard('web')->check()) {
            return redirect()->route('santri.index')->with('success', 'Berhasil menambah data santri');
        } else {
            return redirect()->route('santri.wali')->with('success', 'Berhasil mendaftarkan santri, mohon menunggu persetujuan dari kami');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['santri'] = Santri::findOrFail($id);
        $data['wali'] = Wali::orderBy('nama', 'asc')->get();
        return view('santri.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'wali_id' => 'required',
            'nama' => 'required',
            'tempat_lhr' => 'required',
            'tgl_lhr' => 'required',
            'j_kel' => 'required',
        ]);

        Santri::findOrFail($id)->update($request->toArray());
        return redirect()->route('santri.index')->with('success', 'Berhasil mengubah data santri');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Santri::findOrFail($id)->delete();
        return redirect()->route('santri.index')->with('success', 'Berhasil menghapus data santri');
    }

    public function grafik($id)
    {
        $data['santri'] = Santri::findOrFail($id);
        $nilai = Nilai::where('santri_id', $id)->where('tahun', date('Y'))->get();
        $data['bulan'] = "";
        $data['nilai'] = "";
        foreach($nilai as $n) {
            $data['bulan'] .= getBulanShort()[$n['bulan']].',';
            $data['nilai'] .= $n->rataRataNilai().',';
        }

        $data['bulan'] = rtrim($data['bulan'], ",");
        $data['nilai'] = rtrim($data['nilai'], ",");

        $data['bulanya'] = date('m');
        $bulannya = date('m');

        if(request()->get('b')){
            $data['bulanya'] = request()->get('b');
            $bulannya = request()->get('b');

        }

        $data['absensi'] = Absensi::where('santri_id', $id)->whereRaw('month(tgl) = ' . $bulannya)->orderBy('tgl', 'desc')->get();


        return view('santri.grafik', $data);
    }
}
