<?php

namespace App\Http\Controllers;

use App\KategoriPenilaian;
use Illuminate\Http\Request;

class KategoriPenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('kategori_penilaian.index', ['kategori_penilaian' => KategoriPenilaian::orderBy('nama', 'asc')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kategori_penilaian'] = KategoriPenilaian::getDefaultValues();

        return view('kategori_penilaian.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        KategoriPenilaian::create($request->toArray());
        return redirect()->route('kategori-penilaian.index')->with('success', 'Berhasil menambah data kategori_penilaian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kategori_penilaian'] = KategoriPenilaian::findOrFail($id);
        return view('kategori_penilaian.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        KategoriPenilaian::findOrFail($id)->update($request->toArray());
        return redirect()->route('kategori-penilaian.index')->with('success', 'Berhasil mengubah data kategori_penilaian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        KategoriPenilaian::findOrFail($id)->delete();
        return redirect()->route('kategori-penilaian.index')->with('success', 'Berhasil menghapus data kategori_penilaian');
    }
}
