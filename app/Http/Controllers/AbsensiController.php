<?php

namespace App\Http\Controllers;

use App\Absensi;
use App\Santri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->level === "1") {
            return redirect('dashboard')->with('warning', 'Tidak dapat mengakses halaman');
        }
        $data['absensi'] = [];
        if(!empty(request()->get('tanggal'))) {
            if(Auth::guard('web')->check()) {
                $data['absensi'] = Absensi::where('tgl', request()->get('tanggal'))->get();
            } else {
                $santris = Auth::guard('wali')->user()->getSantri()->get()->pluck('id')->all();
                $data['absensi'] = Absensi::where('tgl', request()->get('tanggal'))->whereIn('santri_id', $santris)->get();
            }
        }
        return view('absensi.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['santri'] = Santri::where('status', '1')->orderBy('nama', 'asc')->get();
        $data['absensi'] = Absensi::getDefaultValues();
        return view('absensi.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->toArray();
        if(isset($input['edit'])) {
            //for edit, delete all the old absensi in this date
            Absensi::where('tgl', $input['tgl'])->delete();
        }
        $data = [];

        $absensi = Absensi::where('tgl', $input['tgl'])->first();

        if(empty($absensi)) {
            for($i = 0; $i < count($input['santri']['id']); $i++) {
                $data[] = [
                    'tgl' => $input['tgl'],
                    'santri_id' => $input['santri']['id'][$i],
                    'status' => $input['santri']['status'][$i],
                    'keterangan' => $input['santri']['keterangan'][$i],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
            Absensi::insert($data);
            if(isset($input['edit'])) {
                return redirect()->back()->with('success', 'Berhasil mengubah data absensi pada tanggal '. $input['tgl']);
            }
            return redirect()->route('absensi.index')->with('success', 'Berhasil menyimpan data absensi');
        }
        return redirect()->route('absensi.index')->with('error', 'Gagal menambah data absensi karena tanggal tersebut sudah terisi, silahkan lakukan update pada absensi di tanggal '. $input['tgl']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
