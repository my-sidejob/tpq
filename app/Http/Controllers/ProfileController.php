<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class profileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('web')->user()->level == "2") {
            return redirect('dashboard')->with('warning', 'Tidak dapat mengakses halaman');
        }
        return view ('profile.index', ['profile' => Profile::orderBy('nama', 'asc')->first()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['profile'] = Profile::getDefaultValues();

        return view('profile.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'no_telp' => 'required',
            'deskripsi' => 'required',
            'logo' => 'required',
        ]);
        $input = $request->toArray();
        $request->file('logo')->store('logo');
        $input['logo'] = $request->file('logo')->hashName();
        $input['user_id'] = Auth::user()->id;
        Profile::create($input);
        return redirect()->route('profile.index')->with('success', 'Berhasil menambah data profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['profile'] = Profile::findOrFail($id);
        return view('profile.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'no_telp' => 'required',
            'deskripsi' => 'required',
        ]);
        $input = $request->toArray();
        if($request->hasFile('logo')) {
            $request->file('logo')->store('logo');
            $input['logo'] = $request->file('logo')->hashName();
        } else {
            unset($input['logo']);
        }
        $input['user_id'] = Auth::user()->id;
        Profile::findOrFail($id)->update($input);
        return redirect()->route('profile.index')->with('success', 'Berhasil mengubah data profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profile::findOrFail($id)->delete();
        return redirect()->route('profile.index')->with('success', 'Berhasil menghapus data profile');
    }
}
