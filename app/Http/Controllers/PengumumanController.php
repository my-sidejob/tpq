<?php

namespace App\Http\Controllers;

use App\Mail\BroadcastPengumuman;
use App\Pengumuman;
use App\Wali;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('web')->check()) {
            $pengumuman = Pengumuman::orderBy('tgl', 'desc')->get();
        } else {
            $pengumuman = Pengumuman::where('status', '1')->orderBy('tgl', 'desc')->get();
        }
        return view ('pengumuman.index', ['pengumuman' => $pengumuman]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pengumuman'] = Pengumuman::getDefaultValues();

        return view('pengumuman.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_pengumuman' => 'required',
            'keterangan' => 'required',
            'tgl' => 'required',
            'status' => 'required'
        ]);

        $input = $request->toArray();
        $input['user_id'] = auth()->user()->id;

        $pengumuman = Pengumuman::create($input);

        $recipients = Wali::all()->pluck('email')->all();
        Mail::to($recipients)
        ->send(new BroadcastPengumuman($pengumuman));

        return redirect()->route('pengumuman.index')->with('success', 'Berhasil menambah data pengumuman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['pengumuman'] = Pengumuman::findOrFail($id);
        return view('pengumuman.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pengumuman'] = Pengumuman::findOrFail($id);
        return view('pengumuman.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_pengumuman' => 'required',
            'keterangan' => 'required',
            'tgl' => 'required',
            'status' => 'required'
        ]);

        Pengumuman::findOrFail($id)->update($request->toArray());
        return redirect()->route('pengumuman.index')->with('success', 'Berhasil mengubah data pengumuman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengumuman::findOrFail($id)->delete();
        return redirect()->route('pengumuman.index')->with('success', 'Berhasil menghapus data pengumuman');
    }
}
