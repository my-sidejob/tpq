<?php

namespace App\Http\Controllers;

use App\Kelompok;
use Illuminate\Http\Request;

class KelompokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('kelompok.index', ['kelompok' => Kelompok::orderBy('nama', 'asc')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kelompok'] = Kelompok::getDefaultValues();

        return view('kelompok.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        Kelompok::create($request->toArray());
        return redirect()->route('kelompok.index')->with('success', 'Berhasil menambah data kelompok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kelompok'] = Kelompok::findOrFail($id);
        return view('kelompok.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        Kelompok::findOrFail($id)->update($request->toArray());
        return redirect()->route('kelompok.index')->with('success', 'Berhasil mengubah data kelompok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kelompok::findOrFail($id)->delete();
        return redirect()->route('kelompok.index')->with('success', 'Berhasil menghapus data kelompok');
    }
}
