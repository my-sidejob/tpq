<?php

namespace App\Http\Controllers;

use App\Wali;
use Illuminate\Http\Request;

class WaliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('wali.index', ['wali' => Wali::orderBy('nama', 'asc')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['wali'] = Wali::getDefaultValues();

        return view('wali.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required|unique:wali,email',
            'password' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
        ]);
        $request['password'] = bcrypt($request['password']);
        Wali::create($request->toArray());
        return redirect()->route('wali.index')->with('success', 'Berhasil menambah data wali');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['wali'] = Wali::findOrFail($id);

        return view('wali.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
        ]);
        $input = $request->all();
        if(empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = bcrypt($input['password']);
        }
        Wali::find($id)->update($input);
        return redirect()->route('wali.index')->with('success', 'Berhasil mengubah data wali');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wali::findOrFail($id)->delete();
        return redirect()->route('wali.index')->with('success', 'Berhasil menghapus data wali');
    }
}
