<?php

namespace App\Http\Controllers;

use App\Santri;
use App\Spp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty($request->dari) && !empty($request->sampai)) {
            $startDate = Carbon::createFromDate($request->dari);
            $endDate = Carbon::createFromDate($request->sampai);

            $q = "(bulan between '" . $startDate->format("m") . "' AND '" . $endDate->format('m') . "') AND tahun IN (".$startDate->format('Y').", ".$endDate->format('Y').")";
        }
        if(Auth::guard('web')->check()) {
            if(!empty($request->dari) && !empty($request->sampai)) {
                $spp =  Spp::whereRaw($q)->orderBy('bulan', 'asc')->orderBy('tahun', 'desc')->get();
            } else {
                $spp =  Spp::orderBy('bulan', 'asc')->orderBy('tahun', 'desc')->get();
            }
            $type = 'default';
        } else {
            $santri = Auth::guard('wali')->user()->getSantri()->get()->pluck('id')->all();
            if(!empty($request->dari) && !empty($request->sampai)) {
                $spp =  Spp::whereRaw($q)->whereIn('santri_id', $santri)->orderBy('bulan', 'asc')->orderBy('tahun', 'desc')->get();
            } else {
                $spp = Spp::whereIn('santri_id', $santri)->orderBy('bulan', 'asc')->orderBy('tahun', 'desc')->get();
            }
            $type = 'wali';
        }

        $input = Spp::getDefaultValues();

        return view ('spp.index', ['spp' => $spp, 'type' => $type, 'input' => $input]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['spp'] = Spp::getDefaultValues();
        if(Auth::guard('web')->check()) {
            $data['santri'] = Santri::orderBy('nama', 'asc')->get();
        } else {
            $data['santri'] = Auth::guard('wali')->user()->getSantri()->where('status', '1')->get();
        }
        return view('spp.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'santri_id' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'nominal' => 'required',
            'bukti_pembayaran' => 'required',
        ]);

        $input = $request->toArray();
        $request->file('bukti_pembayaran')->store('bukti_pembayaran');
        $input['bukti_pembayaran'] = $request->file('bukti_pembayaran')->hashName();
        if(auth()->guard('web') === true) {
            $input['user_id'] = auth()->user()->id;
        }
        Spp::create($input);
        if(Auth::guard('web')->check()) {
            return redirect()->route('spp.index')->with('success', 'Berhasil menambah data SPP');
        } else {
            return redirect()->route('pembayaran-spp.index')->with('success', 'Berhasil menambah data SPP');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['spp'] = Spp::findOrfail($id);
        if(Auth::guard('web')->check()) {
            $data['santri'] = Santri::orderBy('nama', 'asc')->get();
        } else {
            $data['santri'] = Auth::guard('wali')->user()->getSantri()->where('status', '1')->get();
        }
        return view('spp.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'santri_id' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'nominal' => 'required',
        ]);

        $input = $request->toArray();
        Spp::findOrFail($id)->update($input);
        return redirect()->route('spp.index')->with('success', 'Berhasil memperbarui data SPP');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $spp = Spp::findOrFail($id);
        Storage::delete('bukti_pembayaran/'.$spp->bukti_pembayaran);
        $spp->delete();
        return redirect()->route('spp.index')->with('success', 'Berhasil menghapus data spp');
    }

    public function checkMonth($santri_id) {
        $spp_santri = Spp::where('santri_id', $santri_id)->whereIn('status', ["0", "1"])->get()->pluck('bulan')->all();
        $text = "<option value=''>- Pilih bulan -</option>";
        foreach(getBulan() as $key => $option) {
            $text .= "<option value='$key' ". (in_array($key, $spp_santri) ? 'disabled' : '') . " class='text-dark'>" . $option . "</option>";
        }

        return $text;
    }
}
