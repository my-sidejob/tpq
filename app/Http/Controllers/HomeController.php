<?php

namespace App\Http\Controllers;

use App\Santri;
use App\Wali;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function daftar(){
        $data['santri'] = Santri::getDefaultValues();
        $data['wali'] = Wali::getDefaultValues();
        return view('daftar', $data);
    }

    public function store(Request $request) {
        $isWali = false;
        if(!Auth::guard('wali')->check()) {
            $isWali = true;
            $waliValidation = [
                'nama' => 'required',
                'email' => 'required|unique:wali,email',
                'password' => 'required',
                'alamat' => 'required',
                'no_telp' => 'required',
            ];
        } else {
            $waliValidation = [];
        }
        $validation = [
            'nama_santri' => 'required',
            'tempat_lhr_santri' => 'required',
            'tgl_lhr_santri' => 'required',
            'j_kel' => 'required',
        ];

        $validation = array_merge($validation, $waliValidation);

        $request->validate($validation);

        if($isWali) {
            $wali = Wali::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
            ]);

            Auth::guard('wali')->login($wali);
        } else {
            $wali = Auth::guard('wali')->user();
        }
        Santri::create([
            'nama' => $request->nama_santri,
            'tempat_lhr' => $request->tempat_lhr_santri,
            'tgl_lhr' => $request->tgl_lhr_santri,
            'j_kel' => $request->j_kel,
            'wali_id' => $wali->id,
            'status' => "0",
        ]);

        return redirect()->route('santri.wali')->with('success', 'Berhasil mendaftarkan santri, mohon menunggu persetujuan dari kami');
    }

    public function about()
    {
        return view('tentang');
    }
}
