<?php

namespace App\Http\Controllers;

use App\DetailNilai;
use App\KategoriPenilaian;
use App\Kelompok;
use App\Nilai;
use App\Santri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->level === "1") {
            return redirect('dashboard')->with('warning', 'Tidak dapat mengakses halaman');
        }
        if(isset($_GET['bulan']) && isset($_GET['tahun'])){
            if(Auth::guard('web')->check()){
                $data['nilai'] = Nilai::where('bulan', request('bulan'))->where('tahun', request('tahun'))->get();
            } else {
                $santri = Santri::where('wali_id', Auth::user()->id)->get()->pluck('id')->all();
                $data['nilai'] = Nilai::where('bulan', request('bulan'))->where('tahun', request('tahun'))->whereIn('santri_id', $santri)->get();
            }
        } else {
            $data['nilai'] = null;
        }
        return view('nilai.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['santri'] = Santri::where('status', '1')->orderBy('nama', 'asc')->get();
        $data['kelompok'] = Kelompok::orderBy('nama', 'asc')->get();
        $data['nilai'] = Nilai::getDefaultValues();
        $data['kategori_penilaian'] = KategoriPenilaian::orderBy('nama')->get();
        return view('nilai.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'santri_id' => 'required',
            'kelompok_id' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
        ]);
        $input = $request->toArray();
        $nilai = Nilai::create([
            'santri_id' => $request->santri_id,
            'kelompok_id' => $request->kelompok_id,
            'bulan' => $request->bulan,
            'tahun' => $request->tahun,
        ]);
        $detail_nilai = [];
        for($i = 0; $i < count($input['kategori_penilaian_id']); $i++){
            $detail_nilai[] = [
                'nilai_id' => $nilai->id,
                'kategori_penilaian_id' => $input['kategori_penilaian_id'][$i],
                'nilai' => $input['nilai'][$i]
            ];
        }

        DB::table('detail_nilai')->insert($detail_nilai);

        return redirect()->route('nilai.index')->with('success', 'Berhasil menambah nilai santri');
    }

    public function print($id) {
        $data['nilai'] = Nilai::findOrFail($id);
        $data['detail_nilai'] = DetailNilai::where('nilai_id', $id)->get();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('nilai.print', $data);
        return $pdf->stream();
        //return view('nilai.print', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['nilai'] = Nilai::findOrFail($id);
        $data['detail_nilai'] = DetailNilai::where('nilai_id', $id)->get();
        return view('nilai.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['santri'] = Santri::where('status', '1')->orderBy('nama', 'asc')->get();
        $data['kelompok'] = Kelompok::orderBy('nama', 'asc')->get();
        $data['nilai'] = Nilai::findOrFail($id);
        $data['kategori_penilaian'] = KategoriPenilaian::orderBy('nama')->get();
        $data['detail_nilai'] = DetailNilai::where('nilai_id', $id)->get();
        return view('nilai.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'santri_id' => 'required',
            'kelompok_id' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
        ]);
        $input = $request->toArray();
        $nilai = Nilai::findOrFail($id);
        $nilai->update([
            'santri_id' => $request->santri_id,
            'kelompok_id' => $request->kelompok_id,
            'bulan' => $request->bulan,
            'tahun' => $request->tahun,
        ]);
        $detail_nilai = [];
        for($i = 0; $i < count($input['kategori_penilaian_id']); $i++){
            $detail_nilai[] = [
                'nilai_id' => $id,
                'kategori_penilaian_id' => $input['kategori_penilaian_id'][$i],
                'nilai' => $input['nilai'][$i]
            ];
        }

        $nilai->detailNilai()->sync($detail_nilai);

        return redirect()->route('nilai.index')->with('success', 'Berhasil mengubah nilai santri');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Nilai::findOrFail($id)->delete();
        DetailNilai::where('nilai_id', $id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data nilai');
    }
}
