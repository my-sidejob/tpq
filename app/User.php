<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'email',
        'password',
        'j_kel',
        'tempat_lhr',
        'tgl_lhr',
        'alamat',
        'no_telp',
        'level', //1 admin, 2 ustadz/ah
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDefaultValues()
    {
        return [
            'nama' => '',
            'email' => '',
            'password' => '',
            'j_kel' => '',
            'tempat_lhr' => '',
            'tgl_lhr' => '',
            'alamat' => '',
            'no_telp' => '',
            'level' => '',
            'status' => '',
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-secondary">Tidak Aktif</span>';
        }
    }

    public function getLevel() {
        if ($this->level == "1") {
            return '<span class="badge badge-success">Admin</span>';
        } else {
            return '<span class="badge badge-info">Ustadz / Ustadzah</span>';
        }
    }

    public function getLevelAttribute($value) {
        return strval($value);
    }

    public function getStatusAttribute($value) {
        return strval($value);
    }

    public function formatDate() {
        $date = explode('-', $this->tgl_lhr);
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }


}
