<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPenilaian extends Model
{
    protected $table = 'kategori_penilaian';
    protected $fillable = [
        'nama',
        'keterangan',
    ];

    public static function getDefaultValues() {
        return (object) [
            'nama' => '',
            'keterangan' => '',
        ];
    }
}
