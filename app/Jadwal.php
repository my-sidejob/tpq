<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $fillable = [
        'hari',
        'jam_mulai',
        'jam_selesai',
        'keterangan',
        'user_id',
    ];

    public static function getDefaultValues() {
        return (object) [
            'hari' => '',
            'jam_mulai' => '',
            'jam_selesai' => '',
            'keterangan' => '',
        ];
    }
}
