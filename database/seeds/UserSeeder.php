<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama' => "Administrasi",
            'email' => "admin@example.com",
            'password' => Hash::make('secret'),
            "j_kel" => 'l',
            'tempat_lhr' => 'Denpasar',
            'tgl_lhr' => '1990-01-01',
            'alamat' => 'Denpasar',
            'no_telp' => '000',
            'level' => '1',
            'status' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
