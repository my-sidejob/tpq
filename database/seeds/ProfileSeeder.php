<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([
            'nama' => "T. P. A Bina Insani",
            'logo' => 'aG4YrBKvFsd6YkVpOhuYkkN3c2Y8fkD7L40wcLAv.png',
            'email' => 'default@example.com',
            'alamat' => 'jalan di denpasar versi default 10',
            'no_telp' => '08911223344',
            'deskripsi' => 'Lorem ipsum dolor sit amet orem ipsum dolor sit amet lorem ipsum dolor sit amet',
        ]);
    }
}
