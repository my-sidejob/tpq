<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSantriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('santri', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('wali_id');
            $table->string('nama');
            $table->string('tempat_lhr');
            $table->date('tgl_lhr');
            $table->enum('j_kel', ['l', 'p']);
            $table->enum('status', [0, 1])->default(1);
            $table->timestamps();
            $table->foreign('wali_id')->references('id')->on('wali')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('santri');
    }
}
