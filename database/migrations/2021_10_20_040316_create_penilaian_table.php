<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('nilai');

        Schema::create('nilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('kategori_penilaian_id');
            $table->unsignedBigInteger('santri_id');
            $table->unsignedBigInteger('kelompok_id');
            $table->string('nilai');
            $table->string('bulan');
            $table->string('tahun');

            $table->foreign('kategori_penilaian_id')->references('id')->on('kategori_penilaian')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kelompok_id')->references('id')->on('kelompok')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('santri_id')->references('id')->on('santri')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai');
    }
}
