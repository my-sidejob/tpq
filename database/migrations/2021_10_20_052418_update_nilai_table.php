<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nilai', function (Blueprint $table) {
            $table->dropForeign('nilai_kategori_penilaian_id_foreign');
            $table->dropColumn('kategori_penilaian_id');
            $table->dropColumn('nilai');
        });

        Schema::create('detail_nilai', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('nilai_id');
            $table->unsignedBigInteger('kategori_penilaian_id');
            $table->string('nilai');
            $table->timestamps();

            $table->foreign('kategori_penilaian_id')->references('id')->on('kategori_penilaian')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nilai_id')->references('id')->on('nilai')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
