<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('santri_id');
            $table->string('bulan');
            $table->integer('tahun');
            $table->integer('nominal');
            $table->enum('status', [0, 1, 2]); //pending, acc, reject
            $table->string('bukti_pembayaran');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('santri_id')->references('id')->on('santri')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spp');
    }
}
