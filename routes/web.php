<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::middleware(['auth:web'])->group(function(){
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('user', 'UserController');
    Route::resource('wali', 'WaliController');
    Route::resource('santri', 'SantriController');
    Route::resource('kelompok', 'KelompokController');
    Route::resource('profile', 'ProfileController');
    Route::resource('pengumuman', 'PengumumanController');
    Route::resource('jadwal', 'JadwalController');
    Route::resource('spp', 'SppController');
    Route::resource('nilai', 'NilaiController');
    Route::resource('kategori-penilaian', 'KategoriPenilaianController');
    Route::get('print-nilai/{id}', 'NilaiController@print')->name('nilai.print');
    Route::resource('absensi', 'AbsensiController');
    Route::get('santri/grafik/{id}', 'SantriController@grafik')->name('santri.grafik');
});


Route::get('login-wali', function(){
    return view('auth.login-wali');
});

Route::post('login-wali', 'Auth\LoginWaliController@login')->name('login.wali');
Route::post('logout-wali', 'Auth\LoginWaliController@logout')->name('logout.wali');
Route::middleware(['auth:wali'])->group(function(){
    Route::get('dashboard-wali', 'DashboardController@indexWali')->name('dashboard.wali');
    Route::get('santri-wali', 'SantriController@index')->name('santri.wali');
    Route::get('daftar-santri', 'SantriController@create')->name('santri.daftar');
    Route::post('daftar-santri', 'SantriController@store')->name('santri.register');
    Route::get('pembayaran-spp', 'SppController@index')->name('pembayaran-spp.index');
    Route::get('tambah-pembayaran-spp', 'SppController@create')->name('pembayaran-spp.create');
    Route::post('tambah-pembayaran-spp', 'SppController@store')->name('pembayaran-spp.store');
    Route::get('lihat-jadwal', 'JadwalController@index')->name('lihat.jadwal');
    Route::get('lihat-nilai', 'NilaiController@index')->name('lihat.nilai');
    Route::get('lihat-nilai/{id}', 'NilaiController@show')->name('lihat.nilai.detail');
    Route::get('cetak-nilai/{id}', 'NilaiController@print')->name('nilai.cetak');
    Route::get('lihat-pengumuman', 'PengumumanController@index')->name('lihat.pengumuman');
    Route::get('detail-pengumuman/{id}', 'PengumumanController@show')->name('detail.pengumuman');
    Route::get('lihat-absensi', 'AbsensiController@index')->name('lihat.absensi');
    Route::get('grafik-santri/{id}', 'SantriController@grafik')->name('wali.santri.grafik');
});
Route::get('daftar', 'HomeController@daftar')->name('daftar');
Route::post('daftar', 'HomeController@store')->name('daftar.store');
Route::get('tentang-kami', 'HomeController@about');
Route::get('check-month-spp/{santri_id}', 'SppController@checkMonth')->name('check-month');
